import { useState } from 'react';
import './NewExpense.css'
import ExpenseForm from './ExpenseForm';
function NewExpense (props){

    const [isClick, setChecked] = useState(false); 
    
    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        }
        props.onAddExpense(expenseData);
    }
    
    const onAddExpenseClickHandler = () => {
        setChecked(true);
    }

    const onCancelClickHandler = () => {
        setChecked(false);
    }
    return <div className='new-expense'>
        {!isClick && <button onClick={onAddExpenseClickHandler}>Add new expense</button>}
        {isClick && <ExpenseForm onCancel={onCancelClickHandler} onSaveExpenseData = {saveExpenseDataHandler}/>}
    </div>
}
export default NewExpense;