import { useState } from 'react';
import './ExpenseForm.css'

function ExpenseForm(props) {
    // Save value input in state way 1
    const [enteredTitle, setEnteredTitle] = useState('');
    const [enteredAmount, setEnteredAmount] = useState('');
    const [enteredDate, setEnteredDate] = useState('');

    // Event input changed
    const inpTitleChangeHandler = (e) => {
        setEnteredTitle(e.target.value);

    }

    const inpAmountChangeHandler = (e) => {
        setEnteredAmount(e.target.value);
    }

    const inpDateChangeHandler = (e) => {
        setEnteredDate(e.target.value);
    }
    // Event submit
    const submitHandler = (e) => {
        // Prevent default actions of element
        e.preventDefault();

        // Collect data
        const expenseData = {
            title: enteredTitle,
            amount: +enteredAmount,
            date: new Date(enteredDate)
        }
        // Send up data to NewExpense.js
        props.onSaveExpenseData(expenseData);

        props.onCancel();
        // Reset form input
        setEnteredTitle('');
        setEnteredAmount('');
        setEnteredDate('');

    }

    return (
        <form onSubmit={submitHandler}>
            <div className='new-expense__controls'>
                <div className='new-expense__control'>
                    <label>Title</label>
                    <input value={enteredTitle} onChange={inpTitleChangeHandler} type='text' />
                </div>
                <div className='new-expense__control'>
                    <label>Amount</label>
                    <input value={enteredAmount} onChange={inpAmountChangeHandler} type='number' min='0.01' step='0.01' />
                </div>
                <div className='new-expense__control'>
                    <label>Date</label>
                    <input value={enteredDate} onChange={inpDateChangeHandler} type='date' min='2019-01-01' max='2023-12-31' />
                </div>
            </div>
            <div className='new-expense__actions'>
                <button type='submit'>Submit</button>
                <button onClick={props.onCancel}>Cancel</button>
            </div>
        </form>
    )
}
export default ExpenseForm;