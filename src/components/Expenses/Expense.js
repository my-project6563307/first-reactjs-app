import { useState } from 'react';
import './Expense.css'
import Card from '../UI/Card'
import ExpenseFilter from "./ExpenseFilter";
import ExpenseList from './ExpenseList';
import ExpenseChart from './ExpenseChart';
function Expense(props){
    // Set year init for state
    const [selectedYear, setSelectedYear] = useState("2023")

    // Get year selected
    const onSelectedYearHandler = (selectedYearFilter) => {
        setSelectedYear(selectedYearFilter);
    }
    // Filter year selected
    const filterExpensesArr = 
    props.expensesArr.filter(expense => expense.date.getFullYear().toString() === selectedYear)

    return (
        <Card className="expense">
            <ExpenseFilter selected = {selectedYear} onSelectedYear = {onSelectedYearHandler}/>
            <ExpenseChart expenses={filterExpensesArr}/>
            <ExpenseList items={filterExpensesArr}/>
        </Card>
    )
}

export default Expense;