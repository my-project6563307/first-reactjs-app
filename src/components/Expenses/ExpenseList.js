import './ExpenseList.css'
import ExpenseItem from './ExpenseItem';
function ExpenseList (props){
    if(props.items.length === 0){
        return <h2 className='expenses-list__fallback'>No Expenses Found!
        </h2>
    }

    if(props.items.length > 0){
        return (<ul className='expenses-list'>
            {props.items.map(item => <ExpenseItem key={item.id} expense={item}/>)}
        </ul>
        )
    }
}
export default ExpenseList;